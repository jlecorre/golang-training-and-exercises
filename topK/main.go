package main

// This program is able to:
// + Generate a file containing a number K of lines composed of random numbers between 0 and the value of K
// + Read this file to save in memory the content of the previous generated file
// + Display to the user the top K (which is defined as a variable) of the largest numbers found in the file

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"
)

const (
	RootDir        = "dataset"
	FileNamePrefix = "top-k"
	FileExtension  = "txt"
	FileMode       = 0755
	NumberK        = 15000
	TopNumberK     = 10
)

var FileName = RootDir + "/" + strings.Join([]string{FileNamePrefix, FileExtension}, ".")

// CheckIfExists checks if a directory exists and creates it if required
func CheckIfExists(name string) error {
	_, err := os.Stat(name)
	if os.IsNotExist(err) {
		err := os.Mkdir(name, FileMode)
		if err != nil {
			err := fmt.Errorf("can't create the %s repository", RootDir)
			fmt.Println(err)
			return err
		}
	}
	return nil
}

// GenerateRandomNumber returns an array of random integers
func GenerateRandomNumber(number int) []int {
	randomIntegers := make([]int, 0)
	var value int

	// Force the math/rand library to constantly-changing generated number between two execution of the program
	rand.Seed(time.Now().UnixNano())

	for i := 0; i < number; i++ {
		// Generate pseudo-random number between 0 and the value of the parameter given as input
		value = rand.Intn(number)
		randomIntegers = append(randomIntegers, value)
	}

	return randomIntegers
}

// GenerateDataSet generates a file containing a number K of lines composed of random numbers between 0 and the value of K
func GenerateDataSet() {
	// Create the directory if required
	err := CheckIfExists(RootDir)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	} else {
		// Create the dataset file
		CreatedFile, err := os.Create(FileName)
		if err != nil {
			err := fmt.Errorf("can't create the %s file", FileName)
			fmt.Println(err)
			os.Exit(1)
		}

		// Generate the content of the dataset
		for _, value := range GenerateRandomNumber(NumberK) {
			_, err := fmt.Fprintln(CreatedFile, value)
			if err != nil {
				err := fmt.Errorf("can't generate the content of the %s file", FileName)
				fmt.Println(err)
				os.Exit(1)
			}
		}
	}
}

// FindLargestTopKNumbers returns the topK larger numbers found in the previous generated dataset
// TODO - Split this function to improve its readability
func FindLargestTopKNumbers() []int {
	var arrayOfStringsToConvert []string
	var integersToSort []int
	var stringConvertedIntoInteger int

	// Read the content of the previous generated file
	// TODO - Find a way to load chunk of the file in memory instead of the complete file to prevent to memory overloading
	// Example of implementation to possibly follow: https://medium.com/swlh/processing-16gb-file-in-seconds-go-lang-3982c235dfa2
	fileContent, err := os.Open(FileName)
	if err != nil {
		err := fmt.Errorf("can't read the content of the %s file", FileName)
		fmt.Println(err)
	}

	// Create a scanner to manipulate the content of the file
	content := bufio.NewScanner(fileContent)
	// Scan the file content to append it into an array of strings
	for content.Scan() {
		arrayOfStringsToConvert = append(arrayOfStringsToConvert, content.Text())
	}

	// Convert the array of strings into an array of integers
	for _, value := range arrayOfStringsToConvert {
		stringConvertedIntoInteger, err = strconv.Atoi(value)
		if err != nil {
			err := fmt.Errorf("can't convert the content of the %s file", FileName)
			fmt.Println(err)
			os.Exit(1)
		}
		integersToSort = append(integersToSort, stringConvertedIntoInteger)
	}

	// Sort the array in increasing order
	sort.Ints(integersToSort)
	result := integersToSort[len(integersToSort)-TopNumberK:]
	return result
}

func main() {
	GenerateDataSet()
	largestTopKNumbers := FindLargestTopKNumbers()
	fmt.Println(largestTopKNumbers)
}
