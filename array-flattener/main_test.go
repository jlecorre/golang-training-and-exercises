package flattener

import (
	"github.com/magiconair/properties/assert"
	"testing"
)

func TestArrayFlattenerWithNotNestedSlice(t *testing.T) {
	sliceNotNested := []interface{}{1, 2, 3, 4}
	expectedResult := []interface{}{1, 2, 3, 4}

	result := ArrayFlattener(sliceNotNested)
	assert.Equal(t, result, expectedResult)
}

func TestArrayFlattenerWithOneNestedSlice(t *testing.T) {
	sliceNotNested := []interface{}{1, []interface{}{2, 3, 4}, 5}
	expectedResult := []interface{}{1, 2, 3, 4, 5}

	result := ArrayFlattener(sliceNotNested)
	assert.Equal(t, result, expectedResult)
}

func TestArrayFlattenerWithOneNestedSliceAndNilValue(t *testing.T) {
	sliceNotNested := []interface{}{1, []interface{}{2, 3, 4}, nil, 6}
	expectedResult := []interface{}{1, 2, 3, 4, 6}

	result := ArrayFlattener(sliceNotNested)
	assert.Equal(t, result, expectedResult)
}

func TestArrayFlattenerWithOnlyNilValue(t *testing.T) {
	sliceNotNested := []interface{}{nil, nil, nil, nil}
	expectedResult := []interface{}{}

	result := ArrayFlattener(sliceNotNested)
	assert.Equal(t, result, expectedResult)
}

func TestArrayFlattenerWithMultipleNestedSliceAndNilValue(t *testing.T) {
	sliceNotNested := []interface{}{1, []interface{}{2, []interface{}{3, []interface{}{4}}, 5}, nil, []interface{}{6}}
	expectedResult := []interface{}{1, 2, 3, 4, 5, 6}

	result := ArrayFlattener(sliceNotNested)
	assert.Equal(t, result, expectedResult)
}
