package flattener

// This program is able to take a nested slice as input
// to print as output a single flattened slice with all
// values given as parameters
// Example: [[1,2,[3]],4] => [1,2,3,4]

// ArrayFlattener returns a flatten slice
func ArrayFlattener(ArrayToFlat interface{}) (ArrayFlattened []interface{}) {
	ArrayFlattened = make([]interface{}, 0)

	// For each value in the ArrayToFlat slice give as input
	for _, value := range ArrayToFlat.([]interface{}) {
		// Execute instruction depending on its type
		switch value.(type) {
		// If the value is nil don't break the program
		case nil:
			continue
		// If the value is a slice execute the ArrayFlattener function
		case []interface{}:
			ArrayFlattened = append(ArrayFlattened, ArrayFlattener(value)...)
		// Else just append the value in a temporary variable to return
		default:
			ArrayFlattened = append(ArrayFlattened, value)
		}
	}
	return ArrayFlattened
}
