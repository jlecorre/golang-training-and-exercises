# Golang training and exercises

This repository is here to store:

- various exercises
- weird tests
- coding games related to the Golang language
- and others stuff depending on my mood :D

## Array flattener project

This program is able to take a nested slice as input to print as output a single flattened slice with all values given as parameters.  
Example: `[[1,2,[3]],4] => [1,2,3,4]`

## TopK numbers finder

This program is able to:

+ Generate a file containing a number K of lines composed of random numbers between 0 and the value of K
+ Read this file to save in memory the content of the previous generated file
+ Display to the user the top K (which is defined as a variable) of the largest numbers found in the file

## Next project

Any idea to share with me?  
You're welcome :P
